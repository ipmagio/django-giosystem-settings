.. note::

   **This project is now deprecated**

   The code has been moved to the newer ``giosystem-settings`` project and
   can be found on gitlab.com at:

   https://gitlab.com/giosystem/giosystem-settings


This is a django app to store settings for giosystem.

The idea behind this django application is to have the settings for giosystem
available from one single host to all the other hosts involved in the system. 
This way the operational hosts access their settings from a single location.

It is also possible to install this app on another host, tweak the settings
and use them for testing purposes, all while keeipng the operational settings
untouched.

Installation
============

0. Create a virtualenv to hold the installation (This step is
   optional, but highly reccommended).

   .. code:: bash

      virtualenv venv

#. Install the following external dependencies:

   .. code:: bash

      sudo apt-get install graphviz graphviz-dev

#. For a normal installation, just use the setup.py file:

   .. code:: bash

      venv/bin/python setup.py install

#. For a developer installation, use pip and the provided requirements file:

   .. code:: bash

      venv/bin/pip install -r requirements.txt

Usage
=====

Create a new django project and add *coresettings* to the project's installed
apps.
