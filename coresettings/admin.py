from django.contrib import admin
import coresettings.models as cs

class DatasetInline(admin.StackedInline):
    model = cs.Dataset
    extra = 0

class ProductAdmin(admin.ModelAdmin):
    inlines = (DatasetInline,)
    list_display = ('short_name', 'version',)

class GeographicAreaAdmin(admin.ModelAdmin):
    list_display = ('name', 'upper_latitude', 'lower_latitude',
                    'leftmost_longitude', 'rightmost_longitude')

class InputMembershipInline(admin.StackedInline):
    model = cs.InputMembership
    fieldsets = (
        (
            None, 
            {
                'fields': ('input', 'except_hours', 'optional',
                           'description', 'multiplicity'),
            },
        ),
        (
            'Offset timeslot',
            {
                'fields': ('offset_years', 'offset_months', 'offset_days',
                           'offset_hours', 'offset_minutes','offset_decades',),
                'classes': ('collapse',),
            }
        ),
        (
            'Single offset',
            {
                'fields': ('single_offset_years', 'single_offset_months',
                           'single_offset_days', 'single_offset_hours',
                           'single_offset_minutes', 'single_offset_decades',),
                'classes': ('collapse',),
            }
        ),
        (
            'Single age',
            {
                'fields': ('single_age_age', 'single_age_relative',
                           'single_age_fix_year', 'single_age_fix_month',
                           'single_age_fix_day', 'single_age_fix_hour',
                           'single_age_fix_minute',),
                'classes': ('collapse',),
            }
        ),
        (
            'Multiple arbitrary',
            {
                'fields': (
                    'multiple_arbitrary_frequency_offset_years',
                    'multiple_arbitrary_frequency_offset_days',
                    'multiple_arbitrary_frequency_offset_months',
                    'multiple_arbitrary_frequency_offset_hours',
                    'multiple_arbitrary_frequency_offset_minutes',
                    'multiple_arbitrary_start_offset_years',
                    'multiple_arbitrary_start_offset_days',
                    'multiple_arbitrary_start_offset_months',
                    'multiple_arbitrary_start_offset_hours',
                    'multiple_arbitrary_start_offset_minutes',
                    'multiple_arbitrary_end_offset_years',
                    'multiple_arbitrary_end_offset_days',
                    'multiple_arbitrary_end_offset_months',
                    'multiple_arbitrary_end_offset_hours',
                    'multiple_arbitrary_end_offset_minutes',
                ),
                'classes': ('collapse',),
            }
        ),
        (
            'Multiple decadal',
            {
                'fields': ('multiple_decadal_fix_hour',),
                'classes': ('collapse',),
            }
        ),
    )
    extra = 1

class OutputMembershipInline(admin.StackedInline):
    model = cs.OutputMembership
    fieldsets = (
        (
            None, 
            {
                'fields': ('output', 'except_hours', 'optional',
                           'description', 'multiplicity',
                           'should_be_archived', 'should_be_cleaned',
                           'cleaning_offset'),
            },
        ),
        (
            'Offset timeslot',
            {
                'fields': ('offset_years', 'offset_months', 'offset_days',
                           'offset_hours', 'offset_minutes', 'offset_decades',),
                'classes': ('collapse',),
            }
        ),
        (
            'Single offset',
            {
                'fields': ('single_offset_years', 'single_offset_months',
                           'single_offset_days', 'single_offset_hours',
                           'single_offset_minutes', 'single_offset_decades',),
                'classes': ('collapse',),
            }
        ),
        (
            'Single age',
            {
                'fields': ('single_age_age', 'single_age_relative',
                           'single_age_fix_year', 'single_age_fix_month',
                           'single_age_fix_day', 'single_age_fix_hour',
                           'single_age_fix_minute',),
                'classes': ('collapse',),
            }
        ),
        (
            'Multiple arbitrary',
            {
                'fields': (
                    'multiple_arbitrary_frequency_offset_years',
                    'multiple_arbitrary_frequency_offset_days',
                    'multiple_arbitrary_frequency_offset_months',
                    'multiple_arbitrary_frequency_offset_hours',
                    'multiple_arbitrary_frequency_offset_minutes',
                    'multiple_arbitrary_start_offset_years',
                    'multiple_arbitrary_start_offset_days',
                    'multiple_arbitrary_start_offset_months',
                    'multiple_arbitrary_start_offset_hours',
                    'multiple_arbitrary_start_offset_minutes',
                    'multiple_arbitrary_end_offset_years',
                    'multiple_arbitrary_end_offset_days',
                    'multiple_arbitrary_end_offset_months',
                    'multiple_arbitrary_end_offset_hours',
                    'multiple_arbitrary_end_offset_minutes',
                ),
                'classes': ('collapse',),
            }
        ),
        (
            'Multiple decadal',
            {
                'fields': ('multiple_decadal_fix_hour',),
                'classes': ('collapse',),
            }
        ),
    )
    extra = 1

class PackageParameterInline(admin.TabularInline):
    model = cs.PackageParameter
    extra = 1

class FileParameterInline(admin.TabularInline):
    model = cs.FileParameter
    extra = 1

class SourceParameterInline(admin.TabularInline):
    model = cs.SourceParameter
    extra = 1

class SourceSearchPatternInline(admin.TabularInline):
    model = cs.SourceSearchPattern
    extra = 1

class PackageAdmin(admin.ModelAdmin):
    filter_horizontal = ('depends_on',)
    radio_fields = {
        'trigger_type': admin.HORIZONTAL,
        'use_specific_host': admin.HORIZONTAL,
        'specific_host_role': admin.HORIZONTAL,
    }
    inlines = (PackageParameterInline, InputMembershipInline,
               OutputMembershipInline)
    #list_display = ('name', 'core_class', 'category', 'execution_frequency',
    #                'input_names', 'output_names')
    list_display = ('name', 'core_class', 'external_command',)
    search_fields = ('name',)
    list_filter = ('hourly_category', 'daily_category')
    fieldsets = (
        (
            None,
            {
                'fields': ('name', 'description', 
                           'hourly_category', 'daily_category',
                           'exclude_hours',
                           'execute_days_of_the_month', 'core_class',
                           'external_command', 'compress_outputs',
                           'use_specific_host', 'trigger_type',
                           'ecflow_limit', 'ecflow_tries',
                           'ecflow_retry_frequency',)
            }
        ),
        (
            'Trigger type settings',
            {
                'fields': ('depends_on', 'start_time_offset'),
                'classes': ('collapse',)
            }
        ),
        (
            'Specific execution host settings',
             {
                 'fields': (
                     'specific_host',
                     'specific_host_role'
                 ),
                 'classes': ('collapse',)
             }
        ),
    )

class PackageHourlyCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'use_specific_host',)
    radio_fields = {
        'use_specific_host': admin.HORIZONTAL,
        'specific_host_role': admin.HORIZONTAL,
    }

class PackageDailyCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'use_specific_host',)
    radio_fields = {
        'use_specific_host': admin.HORIZONTAL,
        'specific_host_role': admin.HORIZONTAL,
    }


class FileAdmin(admin.ModelAdmin):
    inlines = (FileParameterInline,)
    list_display = ('name', 'search_path', 'system_input', 'file_type',)
    search_fields = ('name',)
    list_filter = ('system_input', 'file_type', 'frequency', 'to_copy',
                   'downloadable',)

class SourceAdmin(admin.ModelAdmin):
    inlines = (SourceSearchPatternInline, SourceParameterInline,)

class SuiteAdmin(admin.ModelAdmin):
    filter_horizontal = ('packages', 'collaborators_to_email',
                         'extra_hosts_to_copy_outputs')
    radio_fields = {
        'date_mode': admin.HORIZONTAL,
    }
    list_display = ("name", "operation_mode", "description", "default_host",
                    "start_date", "end_date")
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "description",
                    "operation_mode",
                    "default_host",
                    "packages",
                    "date_mode",
                    "start_date",
                    "end_date",
                    "date_list",
                    "create_cleaning_family",
                    "create_archive_family",
                ),
            }
        ),
        (
            'operation monitoring settings',
            {
                'fields': (
                    'operations_database_url',
                    'operations_database_user',
                    'operations_database_api_key',
                    'collaborators_to_email',
                ),
                'classes': ('collapse',),
            }
        ),
        (
            'advanced settings',
            {
                'fields': (
                    'use_io_buffer_for_searching',
                    'copy_outputs_to_io_buffer',
                    'use_archive_for_searching',
                    'copy_outputs_to_archive',
                    'extra_hosts_to_copy_outputs',
                    'remove_package_working_dir',
                    'archive_from_io_buffer',
                    'archive_time',
                ),
                'classes': ('collapse',),
            }
        )
    )

class HostAdmin(admin.ModelAdmin):
    list_display = ('name', 'role_names', 'data_dir', 'code_dir', 'temp_dir',)

class CollaboratorInline(admin.TabularInline):
    model = cs.Collaborator
    extra = 1

class OrganizationAdmin(admin.ModelAdmin):
    inlines = (CollaboratorInline,)
    list_display = ('short_name', 'e_mail',)

class HomeOrganizationAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class IOBufferHostAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class ArchiveHostAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class WebserverHostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['host', 'base_url', 'static_quicklook_base_uri'],}),
        ('CSW Catalogue', {
            'fields': ['catalogue_username', 'catalogue_password',
                       'catalogue_base_uri', 'catalogue_csw_search_uri',
                       'catalogue_csw_transaction_uri'],
            },
        ),
        ('OGC mapping services', {'fields': ['ows_base_uri']}),
    ]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class EcflowServerAdmin(admin.ModelAdmin):
    filter_horizontal = ('suites',)
    list_display = ('name', 'host', 'port_number',)

class ExternalFTPHostAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class OrderingHostAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class DataReceiverHostAdmin(admin.ModelAdmin):

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class LoggingAdmin(admin.ModelAdmin):
    list_display = ('log_level', 'log_format',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

class PaletteRuleInline(admin.TabularInline):
    model = cs.PaletteRule
    extra = 1

class MapserverPaletteAdmin(admin.ModelAdmin):
    inlines = (PaletteRuleInline,)

admin.site.register(cs.Product, ProductAdmin)
admin.site.register(cs.GeographicArea, GeographicAreaAdmin)
admin.site.register(cs.HomeOrganization, HomeOrganizationAdmin)
admin.site.register(cs.Organization, OrganizationAdmin)
admin.site.register(cs.Host, HostAdmin)
admin.site.register(cs.PackageHourlyCategory, PackageHourlyCategoryAdmin)
admin.site.register(cs.PackageDailyCategory, PackageDailyCategoryAdmin)
admin.site.register(cs.Suite, SuiteAdmin)
admin.site.register(cs.Package, PackageAdmin)
admin.site.register(cs.File, FileAdmin)
admin.site.register(cs.Source, SourceAdmin)
admin.site.register(cs.IOBufferHost, IOBufferHostAdmin)
admin.site.register(cs.ArchiveHost, ArchiveHostAdmin)
admin.site.register(cs.WebserverHost, WebserverHostAdmin)
admin.site.register(cs.EcflowServer, EcflowServerAdmin)
admin.site.register(cs.ExternalFTPHost, ExternalFTPHostAdmin)
admin.site.register(cs.OrderingHost, OrderingHostAdmin)
admin.site.register(cs.DataReceiverHost, DataReceiverHostAdmin)
admin.site.register(cs.Logging, LoggingAdmin)
admin.site.register(cs.MapserverPalette, MapserverPaletteAdmin)
