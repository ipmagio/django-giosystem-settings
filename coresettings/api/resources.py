from tastypie import fields
from tastypie.constants import ALL_WITH_RELATIONS
from tastypie.resources import ModelResource
from coresettings import models

class GeographicAreaResource(ModelResource):

    class Meta:
        queryset = models.GeographicArea.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']


class CollaboratorResource(ModelResource):
    organization = fields.ToOneField('coresettings.api.resources.Organization'
                                     'Resource', 'organization')

    class Meta:
        queryset = models.Collaborator.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']


class OrganizationResource(ModelResource):
    collaborators = fields.ToManyField('coresettings.api.resources.'
                                       'CollaboratorResource',
                                       'collaborator_set',
                                       related_name='organization',
                                       full=True, null=True)

    class Meta:
        queryset = models.Organization.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class HomeOrganizationResource(ModelResource):
    organization = fields.ToOneField('coresettings.api.resources.Organization'
                                     'Resource', 'organization', full=True)

    class Meta:
        queryset = models.HomeOrganization.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']

class LoggingResource(ModelResource):

    class Meta:
        queryset = models.Logging.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']


class SuiteResource(ModelResource):
    packages = fields.ToManyField('coresettings.api.resources.PackageResource',
                                  'packages', related_name='suite',
                                  null=True)
    extra_hosts_to_copy_outputs = fields.ToManyField('coresettings.api.'
                                                     'resources.HostResource',
                                                     'extra_hosts_to_copy_outputs',
                                                     related_name='suite_extra_hosts',
                                                     null=True)
    default_host = fields.ToOneField('coresettings.api.resources.HostResource',
                                     'default_host', full=True)
    collaborators_to_email = fields.ToManyField('coresettings.api.resources.'
                                                'CollaboratorResource',
                                                'collaborators_to_email',
                                                related_name='suite',
                                                null=True, full=True)

    class Meta:
        queryset = models.Suite.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
        filtering = {'name': 'exact',}


class PackageHourlyCategoryResource(ModelResource):
    specific_host = fields.ToOneField('coresettings.api.resources.Host'
                                      'Resource', 'specific_host',
                                       null=True)

    class Meta:
        queryset = models.PackageHourlyCategory.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id', 'description']

class PackageDailyCategoryResource(ModelResource):
    specific_host = fields.ToOneField('coresettings.api.resources.Host'
                                      'Resource', 'specific_host',
                                       null=True)

    class Meta:
        queryset = models.PackageDailyCategory.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id', 'description']


class PackageResource(ModelResource):
    hourly_category = fields.ToOneField('coresettings.api.resources.PackageHourlyCategory'
                                 'Resource', 'hourly_category', full=True)
    daily_category = fields.ToOneField('coresettings.api.resources.PackageDailyCategory'
                                 'Resource', 'daily_category', full=True)
    depends_on = fields.ToManyField('coresettings.api.resources.Package'
                                    'Resource', 'depends_on',
                                    related_name='dependency', null=True)
    parameters = fields.ToManyField('coresettings.api.resources.Package'
                                    'ParameterResource',
                                    'packageparameter_set',
                                    related_name='package',
                                    full=True,
                                    null=True)
    inputs = fields.ToManyField('coresettings.api.resources.'
                                'InputMembershipResource',
                                'inputmembership_set',
                                related_name='package',
                                full=True,
                                null=True)
    outputs = fields.ToManyField('coresettings.api.resources.'
                                'OutputMembershipResource',
                                'outputmembership_set',
                                related_name='package',
                                full=True,
                                null=True)
    specific_host = fields.ToOneField('coresettings.api.resources.HostResource',
                                   'specific_host', null=True)

    class Meta:
        queryset = models.Package.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
        filtering = {'name': 'exact',}


class InputMembershipResource(ModelResource):
    input = fields.ToOneField('coresettings.api.resources.FileResource',
                              'input')

    class Meta:
        queryset = models.InputMembership.objects.all()
        include_resource_uri = False
        allowed_methods = ['get']
        excludes = ['id']


class OutputMembershipResource(ModelResource):
    output = fields.ToOneField('coresettings.api.resources.FileResource',
                               'output')

    class Meta:
        queryset = models.OutputMembership.objects.all()
        include_resource_uri = False
        allowed_methods = ['get']
        excludes = ['id']


class PackageParameterResource(ModelResource):

    class Meta:
        queryset = models.PackageParameter.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']


class FileResource(ModelResource):
    parameters = fields.ToManyField('coresettings.api.resources.File'
                                    'ParameterResource',
                                    'fileparameter_set',
                                    related_name='file',
                                    full=True,
                                    null=True)
    product = fields.ToOneField('coresettings.api.resources.ProductResource',
                                'product', null=True)
    source = fields.ToOneField('coresettings.api.resources.SourceResource',
                               'source', null=True)

    class Meta:
        queryset = models.File.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
        max_limit = None
        filtering = {
            'name': ('exact', 'in',),
            'file_type': ('iexact', 'in',),
            'product': ALL_WITH_RELATIONS,
        }


class FileParameterResource(ModelResource):

    class Meta:
        queryset = models.FileParameter.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']


class PaletteRuleResource(ModelResource):

    class Meta:
        queryset = models.PaletteRule.objects.all()
        allowed_methods = ['get']
        include_resource_uri = False
        excludes = ['id']

class MapserverPaletteResource(ModelResource):
    rules = fields.ToManyField('coresettings.api.resources.PaletteRule'
                               'Resource',
                               'paletterule_set',
                               related_name='rule',
                               full=True,
                               null=True)

    class Meta:
        queryset = models.MapserverPalette.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class DatasetResource(ModelResource):

    class Meta:
        queryset = models.Dataset.objects.all()
        allowed_methods = ['get']

class ProductResource(ModelResource):
    #files = fields.ToManyField('coresettings.api.resources.FileResource',
    #                           'file_set', null=True)
    downloadable_files = fields.ToManyField(
        'coresettings.api.resources.FileResource',
        attribute=lambda f: models.File.objects.filter(downloadable=True),
        null=True,
        full=True
    )
    palette = fields.ToOneField('coresettings.api.resources.MapserverPalette'
                                'Resource', 'palette', null=True)
    point_of_contact = fields.ToOneField('coresettings.api.resources.'
                                         'CollaboratorResource',
                                         'point_of_contact',
                                         related_name='product',
                                         full=True)
    principal_investigator = fields.ToOneField('coresettings.api.resources.'
                                               'CollaboratorResource',
                                               'principal_investigator',
                                               related_name='product',
                                               full=True)
    distributor = fields.ToOneField('coresettings.api.resources.'
                                    'CollaboratorResource',
                                    'distributor',
                                    related_name='product',
                                    full=True)
    responsible = fields.ToOneField('coresettings.api.resources.'
                                    'CollaboratorResource',
                                    'responsible',
                                    related_name='product',
                                    full=True)
    owner = fields.ToOneField('coresettings.api.resources.'
                              'CollaboratorResource',
                              'owner',
                              related_name='product',
                              full=True)
    originator = fields.ToOneField('coresettings.api.resources.'
                              'CollaboratorResource',
                              'originator',
                              related_name='product',
                              full=True)
    datasets = fields.ToManyField('coresettings.api.resources.DatasetResource',
                                  'dataset_set', related_name='product',
                                  full=True)

    class Meta:
        queryset = models.Product.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
        filtering = {'short_name': 'exact',}


class SourceResource(ModelResource):
    search_patterns = fields.ToManyField('coresettings.api.resources.Source'
                                         'SearchPatternResource',
                                         'sourcesearchpattern_set',
                                         related_name='source',
                                         full=True,
                                         null=True)
    parameters = fields.ToManyField('coresettings.api.resources.Source'
                                    'ParameterResource',
                                    'sourceparameter_set',
                                    related_name='source',
                                    full=True,
                                    null=True)

    class Meta:
        queryset = models.Source.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
        filtering = {'name': 'exact',}


class SourceSearchPatternResource(ModelResource):

    class Meta:
        queryset = models.SourceSearchPattern.objects.all()
        include_resource_uri = False
        allowed_methods = ['get']
        excludes = ['id']


class SourceParameterResource(ModelResource):

    class Meta:
        queryset = models.SourceParameter.objects.all()
        include_resource_uri = False
        allowed_methods = ['get']
        excludes = ['id']


class HostResource(ModelResource):

    class Meta:
        queryset = models.Host.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
        filtering = {'name': 'exact',}


class IOBufferHostResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')

    class Meta:
        queryset = models.IOBufferHost.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class ArchiveHostResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')

    class Meta:
        queryset = models.ArchiveHost.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class WebserverHostResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')

    class Meta:
        queryset = models.WebserverHost.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class EcflowServerResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')
    suites = fields.ToManyField('coresettings.api.resources.SuiteResource',
                                'suites', related_name='ecflow_server',
                                null=True)

    class Meta:
        queryset = models.EcflowServer.objects.all()
        allowed_methods = ['get']


class ExternalFTPHostResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')

    class Meta:
        queryset = models.ExternalFTPHost.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class OrderingHostResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')

    class Meta:
        queryset = models.OrderingHost.objects.all()
        allowed_methods = ['get']
        excludes = ['id']


class DataReceiverHostResource(ModelResource):
    host = fields.ToOneField('coresettings.api.resources.HostResource', 'host')

    class Meta:
        queryset = models.DataReceiverHost.objects.all()
        allowed_methods = ['get']
        excludes = ['id']
