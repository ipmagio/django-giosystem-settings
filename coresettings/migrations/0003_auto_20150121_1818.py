# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0002_file_testing'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='testing',
            field=models.BooleanField(default=False, help_text=b'Is this file used for testing purposes? This option is only used when creating instances using a file_name, such as when using the GioFile.from_file_name() method. This method is usually used with a file_name gotten directly from the CSW catalogue and thus, since testing files are not meant to be inserted into the catalogue, we usually do not want the from_file_name() method to taking this file into account. "Testing" files should be avoided in the production settings. A better alternative is to just define a new set of settings and use those for testing.'),
            preserve_default=True,
        ),
    ]
