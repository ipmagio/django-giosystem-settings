# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0003_auto_20150121_1818'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='ecflow_tries',
            field=models.IntegerField(default=1, help_text=b'How many times should an ecflow task that uses this package try to execute it with a successfull result?', null=True, blank=True),
            preserve_default=True,
        ),
    ]
