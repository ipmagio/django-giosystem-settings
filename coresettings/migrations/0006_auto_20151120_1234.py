# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0005_auto_20151120_1155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='file_type',
            field=models.CharField(default=b'hdf5', max_length=10, choices=[(b'hdf5', b'hdf5'), (b'NetCDF', b'NetCDF'), (b'lrit', b'lrit'), (b'hrit', b'hrit'), (b'grib', b'grib'), (b'bufr', b'bufr'), (b'geotiff', b'geotiff'), (b'png', b'png'), (b'mapfile', b'mapfile'), (b'txt', b'txt'), (b'xml', b'xml'), (b'dgg', b'dgg'), (b'bin', b'bin')]),
        ),
    ]
