# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0006_auto_20151120_1234'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='outputmembership',
            name='cleaning_offset_end',
        ),
        migrations.RemoveField(
            model_name='outputmembership',
            name='cleaning_offset_start',
        ),
        migrations.AddField(
            model_name='outputmembership',
            name='cleaning_offset',
            field=models.IntegerField(default=1, help_text=b"Number of days to subtract from the input's timeslot in order to perform a safe deletion by the autogenerated cleanup task. This option is only used if the 'should be cleaned' option is active"),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='outputmembership',
            name='should_be_archived',
            field=models.BooleanField(default=True, help_text=b'Should this output be archived by the autogenerated archive task?'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='outputmembership',
            name='should_be_cleaned',
            field=models.BooleanField(default=True, help_text=b'Should this output be cleaned by the autogenerated cleanup task?'),
            preserve_default=True,
        ),
    ]
