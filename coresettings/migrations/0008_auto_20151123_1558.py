# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0007_auto_20151120_1456'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suite',
            name='archive_frequency',
        ),
        migrations.RemoveField(
            model_name='suite',
            name='cleaning_frequency_days',
        ),
    ]
