# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0008_auto_20151123_1558'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='suite',
            name='compress_outputs',
        ),
        migrations.AddField(
            model_name='package',
            name='compress_outputs',
            field=models.BooleanField(default=False, help_text=b'Should the outputs be compressed with bz2?'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='package',
            name='ecflow_tries',
            field=models.IntegerField(default=1, help_text=b'How many times should an ecflow task that uses this package try to execute it with a successful result?', null=True, blank=True),
            preserve_default=True,
        ),
    ]
