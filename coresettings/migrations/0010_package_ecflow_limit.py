# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0009_auto_20151126_1421'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='ecflow_limit',
            field=models.CharField(default=b'normal tasks', help_text=b'Ecflow limit for the tasks that use this package', max_length=50, choices=[(b'normal tasks', b'normal tasks'), (b'fast tasks', b'fast tasks')]),
            preserve_default=True,
        ),
    ]
