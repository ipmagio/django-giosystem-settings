# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0010_package_ecflow_limit'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='execute_days_of_the_month',
            field=models.CommaSeparatedIntegerField(help_text=b'Specify which days of the month this package should be executed. It is assumed that the task should run every days, if no value is given.', max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='host',
            name='ip',
            field=models.GenericIPAddressField(),
        ),
        migrations.AlterField(
            model_name='package',
            name='depends_on',
            field=models.ManyToManyField(help_text=b'If a dependency based trigger is chosen, on which other packages does this one depend on?', to='coresettings.Package', blank=True),
        ),
        migrations.AlterField(
            model_name='suite',
            name='collaborators_to_email',
            field=models.ManyToManyField(help_text=b'List of people that will be notified via e-mail when a task fails or returns a false result', to='coresettings.Collaborator', blank=True),
        ),
        migrations.AlterField(
            model_name='suite',
            name='extra_hosts_to_copy_outputs',
            field=models.ManyToManyField(help_text=b"Send the package's outputs to extra hosts", to='coresettings.Host', blank=True),
        ),
        migrations.AlterField(
            model_name='suite',
            name='packages',
            field=models.ManyToManyField(help_text=b'choose the packages that are part of this suite', to='coresettings.Package', blank=True),
        ),
    ]
