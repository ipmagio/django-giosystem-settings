# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0011_auto_20151130_1447'),
    ]

    operations = [
        migrations.CreateModel(
            name='PackageDailyCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('description', models.TextField(blank=True)),
                ('use_specific_host', models.CharField(blank=True, max_length=50, null=True, help_text=b"Should the packages in this category be executed in a specific host? If so, choose between specifying the host or a specific host role. This setting overrides the definition for the suite's default host.", choices=[(b'host', b'host'), (b'host role', b'host role')])),
                ('specific_host_role', models.CharField(blank=True, max_length=100, null=True, help_text=b'Host role to use when determining where to execute the packages that belong to this category. This parameter is only valid if "host role" is chosen from the "use_specific_host" option.', choices=[(b'io buffer', b'io buffer'), (b'web server', b'web server'), (b'archive', b'archive'), (b'external FTP server', b'external FTP server'), (b'data receiver', b'data receiver')])),
                ('specific_host', models.ForeignKey(blank=True, to='coresettings.Host', help_text=b'Host to use when running the packages that belong to this category. This parameter is only valid if "host" is chosen from the "use_specific_host" option.', null=True)),
            ],
            options={
                'verbose_name_plural': 'Package Daily Categories',
            },
        ),
        migrations.CreateModel(
            name='PackageHourlyCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('description', models.TextField(blank=True)),
                ('use_specific_host', models.CharField(blank=True, max_length=50, null=True, help_text=b"Should the packages in this category be executed in a specific host? If so, choose between specifying the host or a specific host role. This setting overrides the definition for the suite's default host.", choices=[(b'host', b'host'), (b'host role', b'host role')])),
                ('specific_host_role', models.CharField(blank=True, max_length=100, null=True, help_text=b'Host role to use when determining where to execute the packages that belong to this category. This parameter is only valid if "host role" is chosen from the "use_specific_host" option.', choices=[(b'io buffer', b'io buffer'), (b'web server', b'web server'), (b'archive', b'archive'), (b'external FTP server', b'external FTP server'), (b'data receiver', b'data receiver')])),
                ('specific_host', models.ForeignKey(blank=True, to='coresettings.Host', help_text=b'Host to use when running the packages that belong to this category. This parameter is only valid if "host" is chosen from the "use_specific_host" option.', null=True)),
            ],
            options={
                'verbose_name_plural': 'Package Hourly Categories',
            },
        ),
        migrations.AddField(
            model_name='package',
            name='daily_category',
            field=models.ForeignKey(to='coresettings.PackageDailyCategory', help_text=b'The daily category in which to include this package when generating automatic processing lines.', null=True),
        ),
        migrations.AddField(
            model_name='package',
            name='hourly_category',
            field=models.ForeignKey(to='coresettings.PackageHourlyCategory', help_text=b'The hourly category in which to include this package when generating automatic processing lines.', null=True),
        ),
    ]
