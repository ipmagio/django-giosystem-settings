# -*- coding: utf-8 -*-

# This migration file was created empty, with the command below:
#     django-admin makemigrations --settings=config.settings --empty coresettings
#
# After its creation it has been edited in order to implement the
# "copy_categories" function here used for data migration.

from __future__ import unicode_literals

from django.db import models, migrations

def copy_categories(apps, schema_editor):
    Package = apps.get_model("coresettings", "Package")
    PackageCategory = apps.get_model("coresettings", "PackageCategory")
    PackageHourlyCategory = apps.get_model("coresettings", "PackageHourlyCategory")
    PackageDailyCategory = apps.get_model("coresettings", "PackageDailyCategory")
    for category in PackageCategory.objects.all():
        if not any(PackageHourlyCategory.objects.filter(name=category.name)):
            cat_cp = PackageHourlyCategory(name=category.name, description=category.description)
            if category.specific_host is not None:
                cat_cp.specific_host = category.specific_host
            if category.specific_host_role is not None:
                cat_cp.specific_host_role = category.specific_host_role
            if category.use_specific_host is not None:
                cat_cp.use_specific_host = category.use_specific_host
            cat_cp.save()

    for package in Package.objects.all():
        if package.hourly_category is None:
            h_cat = PackageHourlyCategory.objects.get(name=package.category.name)
            package.hourly_category = h_cat
            package.save()

    for package in Package.objects.all():
        if package.daily_category is None:
            cat_name = package.execution_frequency
            d_cat = PackageDailyCategory.objects.filter(name=cat_name)
            if not any(d_cat):
                d_cat = PackageDailyCategory(name=cat_name)
                d_cat.save()
            else:
                d_cat = d_cat.first()
            package.daily_category = d_cat
            if package.daily_category.name == "decadal daily":
                package.exclude_hours="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23"
            elif package.daily_category.name == "half-daily":
                package.exclude_hours="1,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23"
            package.save()

class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0012_auto_20151202_1527'),
    ]

    operations = [
        migrations.RunPython(copy_categories),
    ]
