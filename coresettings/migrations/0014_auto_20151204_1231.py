# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coresettings', '0013_auto_20151202_1615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='packagecategory',
            name='specific_host',
        ),
        migrations.RemoveField(
            model_name='package',
            name='category',
        ),
        migrations.RemoveField(
            model_name='package',
            name='execution_frequency',
        ),
        migrations.DeleteModel(
            name='PackageCategory',
        ),
    ]
