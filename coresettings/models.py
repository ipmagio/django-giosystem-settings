import datetime as dt

from django.db import models
from django.core.exceptions import ObjectDoesNotExist

import networkx as nx
import pygraphviz as pgv


# choices for common model fields between Package and PackageCategory
HOST = 'host'
HOST_ROLE = 'host role'
SPECIFIC_HOST_CHOICES = (
    (HOST, HOST),
    (HOST_ROLE, HOST_ROLE),
)
IO_BUFFER_HOST = 'io buffer'
WEB_SERVER_HOST = 'web server'
ARCHIVE_HOST = 'archive'
DATA_RECEIVER_HOST = 'data receiver'
EXTERNAL_FTP_SERVER_HOST = 'external FTP server'
SPECIFIC_ROLE_CHOICES = (
    (IO_BUFFER_HOST, IO_BUFFER_HOST),
    (WEB_SERVER_HOST, WEB_SERVER_HOST),
    (ARCHIVE_HOST, ARCHIVE_HOST),
    (EXTERNAL_FTP_SERVER_HOST, EXTERNAL_FTP_SERVER_HOST),
    (DATA_RECEIVER_HOST, DATA_RECEIVER_HOST),
)


class File(models.Model):
    HDF5 = 'hdf5'
    NETCDF = 'NetCDF'
    LRIT = 'lrit'
    HRIT = 'hrit'
    GRIB = 'grib'
    BUFR = 'bufr'
    GTIFF = 'geotiff'
    PNG = 'png'
    MAP = 'mapfile'
    TEXT = 'txt'
    XML = 'xml'
    DGG = 'dgg'
    BIN = 'bin'
    FILE_TYPE_CHOICES = (
        (HDF5, HDF5),
        (NETCDF, NETCDF),
        (LRIT, LRIT),
        (HRIT, HRIT),
        (GRIB, GRIB),
        (BUFR, BUFR),
        (GTIFF, GTIFF),
        (PNG, PNG),
        (MAP, MAP),
        (TEXT, TEXT),
        (XML, XML),
        (DGG, DGG),
        (BIN, BIN),
    )
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    system_input = models.BooleanField(default=False, help_text='Is this file '
                                       'an external input to the giosystem? '
                                       'If so, then its search paths will use '
                                       'the base_path of the data receiver '
                                       'host')
    file_type = models.CharField(max_length=10, choices=FILE_TYPE_CHOICES,
                                 default=HDF5)
    to_copy = models.BooleanField(default=True, help_text='Should this file '
                                  'be copied into a package\' working '
                                  'directory when preparing for processing?')
    STATIC = 'static'
    DYNAMIC = 'dynamic'
    FREQUENCY_CHOICES = (
        (DYNAMIC, DYNAMIC),
        (STATIC, STATIC),
    )
    frequency = models.CharField(max_length=50, choices=FREQUENCY_CHOICES,
                                 default=DYNAMIC, help_text='%s files are '
                                 'only updated by manual intervention, while '
                                 '%s files are continuously read by the '
                                 'system' % (STATIC, DYNAMIC))
    search_pattern = models.CharField(max_length=200, help_text='A regular '
                                      'expression string with added special '
                                      'marks to use when parsing')
    search_path = models.CharField(max_length=200, blank=True, help_text='A '
                                   'regular expression string with added '
                                   'special marks to use when parsing. It '
                                   'specifies the search path to use when '
                                   'looking for this file in the host\'s '
                                   'filesystem. The value in this field will '
                                   'be added to whatever output directories '
                                   'there may be defined in the package that '
                                   'originates this file, if there is one.')
    product = models.ForeignKey('Product', null=True, blank=True)
    downloadable = models.BooleanField(default=False, help_text='Is this file '
                                       'to be made available for download? '
                                       'This option is only meaningful is the '
                                       'file also has an associated product.')
    testing = models.BooleanField(default=False, help_text='Is this file '
                                  'used for testing purposes? This option is '
                                  'only used when creating instances '
                                  'using a file_name, such as when using '
                                  'the GioFile.from_file_name() method. This '
                                  'method is usually used with a file_name '
                                  'gotten directly from the CSW catalogue and '
                                  'thus, since testing files are not meant to '
                                  'be inserted into the catalogue, we usually '
                                  'do not want the from_file_name() method to '
                                  'taking this file into account. "Testing" '
                                  'files should be avoided in the production '
                                  'settings. A better alternative is to just '
                                  'define a new set of settings and use those '
                                  'for testing.')
    source = models.ForeignKey('Source', null=True, blank=True)

    def __unicode__(self):
        return self.name


class FileParameter(models.Model):
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100)
    file = models.ForeignKey(File)

    class Meta:
        verbose_name_plural = 'parameters'

    def __unicode__(self):
        return '%s: %s' % (self.name, self.value)

class PackageHourlyCategory(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    use_specific_host = models.CharField(max_length=50,
                                         choices=SPECIFIC_HOST_CHOICES,
                                         blank=True, null=True,
                                         help_text='Should the packages in '
                                         'this category be executed in a '
                                         'specific host? If so, choose '
                                         'between specifying the host '
                                         'or a specific host role. This '
                                         'setting overrides the definition '
                                         'for the suite\'s default host.')
    specific_host = models.ForeignKey('Host', null=True, blank=True,
                                      help_text='Host to use when running '
                                      'the packages that belong to this '
                                      'category. This parameter is only '
                                      'valid if "%s" is chosen from the '
                                      '"use_specific_host" option.' % HOST)
    specific_host_role = models.CharField(max_length=100, null=True, blank=True,
                                          choices=SPECIFIC_ROLE_CHOICES,
                                          help_text='Host role to use when '
                                          'determining where to execute the '
                                          'packages that belong to this '
                                          'category. This parameter is only '
                                          'valid if "%s" is chosen from the '
                                          '"use_specific_host" option.' % 
                                          HOST_ROLE)
    class Meta:
        verbose_name_plural = 'Package Hourly Categories'

    def __unicode__(self):
        return self.name


class PackageDailyCategory(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    use_specific_host = models.CharField(max_length=50,
                                         choices=SPECIFIC_HOST_CHOICES,
                                         blank=True, null=True,
                                         help_text='Should the packages in '
                                         'this category be executed in a '
                                         'specific host? If so, choose '
                                         'between specifying the host '
                                         'or a specific host role. This '
                                         'setting overrides the definition '
                                         'for the suite\'s default host.')
    specific_host = models.ForeignKey('Host', null=True, blank=True,
                                      help_text='Host to use when running '
                                      'the packages that belong to this '
                                      'category. This parameter is only '
                                      'valid if "%s" is chosen from the '
                                      '"use_specific_host" option.' % HOST)
    specific_host_role = models.CharField(max_length=100, null=True, blank=True,
                                          choices=SPECIFIC_ROLE_CHOICES,
                                          help_text='Host role to use when '
                                          'determining where to execute the '
                                          'packages that belong to this '
                                          'category. This parameter is only '
                                          'valid if "%s" is chosen from the '
                                          '"use_specific_host" option.' % 
                                          HOST_ROLE)

    class Meta:
        verbose_name_plural = 'Package Daily Categories'

    def __unicode__(self):
        return self.name


class Package(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    inputs = models.ManyToManyField(File, through='InputMembership',
                                    related_name='inp+')
    outputs = models.ManyToManyField(File, through='OutputMembership',
                                     related_name='out+')
    hourly_category = models.ForeignKey(PackageHourlyCategory, null=True,
                                        help_text='The hourly category in '
                                        'which to include this package when '
                                        'generating automatic processing lines.')
    daily_category = models.ForeignKey(PackageDailyCategory, null=True,
                                       help_text='The daily category in '
                                       'which to include this package when '
                                       'generating automatic processing lines.')
    FAST_TASKS_LIMIT = "fast tasks"
    NORMAL_TASKS_LIMIT = "normal tasks"
    LIMIT_CHOICES = (
        (NORMAL_TASKS_LIMIT, NORMAL_TASKS_LIMIT),
        (FAST_TASKS_LIMIT, FAST_TASKS_LIMIT),
    )
    ecflow_limit = models.CharField(max_length=50,
                                    default=NORMAL_TASKS_LIMIT,
                                    choices=LIMIT_CHOICES,
                                    help_text="Ecflow limit for the tasks "
                                              "that use this package")
    exclude_hours = models.CommaSeparatedIntegerField(
        max_length=100,
        help_text='For hourly based families, specify if this package should '
                  'be excluded from some hours',
        blank=True
    )
    execute_days_of_the_month = models.CommaSeparatedIntegerField(
        max_length=100,
        help_text='Specify which days of the month this package should '
                  'be executed. It is assumed that the task should run '
                  'every days, if no value is given.',
        blank=True
    )
    core_class = models.CharField(max_length=100, blank=True,
                                  help_text='Full Python path to the class '
                                  'that uses these settings. This is a Python '
                                  'import path and not a normal filesystem '
                                  'path. Example: giosystemcore.packages.'
                                  'basepackage.GioPackage')
    external_command = models.CharField(max_length=100, blank=True, 
                                        help_text='If this package relies on '
                                        'an external command to perform its '
                                        'processing, input here the name of '
                                        'this external command.')
    use_specific_host = models.CharField(max_length=50,
                                         choices=SPECIFIC_HOST_CHOICES,
                                         blank=True,
                                         help_text='Should this package be '
                                         'executed in a specific host? If so, '
                                         'choose between specifying the host '
                                         'or a specific host role. This '
                                         'setting overrides the definition '
                                         'for the category specific host and '
                                         'for the suite\'s default host.')
    ecflow_tries = models.IntegerField(null=True, blank=True, default=1,
                                       help_text='How many times should '
                                       'an ecflow task that uses this '
                                       'package try to execute it with a '
                                       'successful result?')
    ecflow_retry_frequency = models.IntegerField(
        null=True, blank=True, default=10, 
        help_text='Number of minutes that an ecflow task must wait before '
                  'trying to reexecute a failed task. This option is only '
                  'meaningful when the "ecflow_tries" field is greater than '
                  'zero.'
    )
    compress_outputs = models.BooleanField(default=False,
                                           help_text="Should the outputs be "
                                                     "compressed with bz2?")
    specific_host = models.ForeignKey('Host', null=True, blank=True,
                                      help_text='Host to use when running '
                                      'this package. This parameter is only '
                                      'valid if "%s" is chosen from the '
                                      '"use_specific_host" option.' % HOST)
    specific_host_role = models.CharField(max_length=100, null=True, blank=True,
                                          choices=SPECIFIC_ROLE_CHOICES,
                                          help_text='Host role to use when '
                                          'determining where to execute this '
                                          'package. This parameter is only '
                                          'valid if "%s" is chosen from the '
                                          '"use_specific_host" option.' % 
                                          HOST_ROLE)
    TIME_TRIGGER = 'time'
    DEPENDENCY_TRIGGER = 'dependency'
    TRIGGER_TYPE_CHOICES = (
        (TIME_TRIGGER, TIME_TRIGGER),
        (DEPENDENCY_TRIGGER, DEPENDENCY_TRIGGER),
    )
    trigger_type = models.CharField(max_length=50, help_text='What type of '
                                    'trigger should this package have, if any?'
                                    ' Time based trigger means that the '
                                    'package will run when it is time. '
                                    'Dependency based trigger means that the '
                                    'package will run when the dependencies '
                                    'are complete.',
                                    choices=TRIGGER_TYPE_CHOICES,
                                    default=DEPENDENCY_TRIGGER,
                                    null=True, blank=True)
    depends_on = models.ManyToManyField('self', symmetrical=False,
                                        blank=True, help_text='If a '
                                        'dependency based trigger is chosen, '
                                        'on which other packages does this '
                                        'one depend on?')
    start_time_offset = models.SmallIntegerField(help_text='If a time based '
                                                 'trigger is chosen, how many '
                                                 'hours should the family\'s '
                                                 'hour variable be offset '
                                                 'when defining the time '
                                                 'trigger?', default=0)

    def input_names(self):
        name_counts = dict()
        for inp in self.inputs.all():
            if inp.name in name_counts.keys():
                name_counts[inp.name] += 1
            else:
                name_counts[inp.name] = 1
        result = ''
        for name, count in name_counts.iteritems():
            if count == 1:
                result = ', '.join((result, name))
            else:
                result = ', '.join((result, '%s (x%i)' % (name, count)))
        result = result[2:]  # trimming the first comma and space
        return result
    input_names.short_description = 'Inputs'

    def output_names(self):
        name_counts = dict()
        for out in self.outputs.all():
            if out.name in name_counts.keys():
                name_counts[out.name] += 1
            else:
                name_counts[out.name] = 1
        result = ''
        for name, count in name_counts.iteritems():
            if count == 1:
                result = ', '.join((result, name))
            else:
                result = ', '.join((result, '%s (x%i)' % (name, count)))
        result = result[2:]  # trimming the first comma and space
        return result
    output_names.short_description = 'Outputs'

    @classmethod
    def create_graph(cls):
        g = nx.DiGraph()
        for pack in cls.objects.all():
            g.add_node(pack.name, type_='package',
                       category=pack.category.name)
            for dependency in pack.depends_on.all():
                g.add_edge(dependency.name, pack.name)
            for inp in pack.inputs.all():
                g.add_node(inp.name, type_='file', is_input=True,
                           is_output=False)
                g.add_edge(inp.name, pack.name)
            for out in pack.outputs.all():
                g.add_node(out.name, type_='file', is_output=True)
                g.add_edge(pack.name, out.name)
        return g

    def __unicode__(self):
        return self.name


class PackageParameter(models.Model):
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100)
    package = models.ForeignKey(Package)

    class Meta:
        verbose_name_plural = 'parameters'

    def __unicode__(self):
        return '%s: %s' % (self.name, self.value)


class InputMembership(models.Model):
    package = models.ForeignKey(Package)
    input = models.ForeignKey(File)
    optional = models.BooleanField(default=False, help_text='Is this an '
                                   'optional input? Can the package run '
                                   'successfully without it?')
    description = models.TextField(blank=True, help_text='Description of this '
                                   'input in the context of the package.')
    except_hours = models.CommaSeparatedIntegerField(
        max_length=100,
        help_text='A list of hours, separated by commas in which this input '
                  'is unavailable',
        blank=True,
    )
    offset_years = models.SmallIntegerField(default=0, help_text='How many '
                                            'years should be used to offset '
                                            'the package\'s timeslot in '
                                            'order to obtain this input\'s '
                                            'own timeslot?')
    offset_months = models.SmallIntegerField(default=0, help_text='How many '
                                             'months should be used to '
                                             'offset the package\'s '
                                             'timeslot in order to obtain '
                                             'this input\'s own timeslot?')
    offset_days = models.SmallIntegerField(default=0, help_text='How many '
                                           'days should be used to offset '
                                           'the package\'s timeslot in '
                                           'order to obtain this input\'s '
                                           'own timeslot?')
    offset_hours = models.SmallIntegerField(default=0, help_text='How many '
                                            'hours should be used to offset '
                                            'the package\'s timeslot in '
                                            'order to obtain this input\'s '
                                            'own timeslot?')
    offset_minutes = models.SmallIntegerField(default=0, help_text='How '
                                              'many minutes should be used '
                                              'to offset the package\'s '
                                              'timeslot in order to obtain '
                                              'this input\'s own timeslot?')
    offset_decades = models.SmallIntegerField(
        default=0,
        help_text='How many decades should the package\'s timeslot be ' 
                  'offset in order to obtain this input\'s own timeslot'
    )
    SINGLE_OFFSET = 'single_offset'
    SINGLE_AGE = 'single_age'
    MULTIPLE_ARBITRARY = 'multiple_arbitrary'
    MULTIPLE_DECADAL = 'multiple_decadal'
    MULTIPLICITY_CHOICES = (
        (SINGLE_OFFSET, 'single input with further timeslot offsets'),
        (SINGLE_AGE, 'single input with relative age displacement'),
        (MULTIPLE_ARBITRARY, 'multiple inputs with arbitrary interval'),
        (MULTIPLE_DECADAL, 'multiple inputs with decadal interval'),
    )
    multiplicity = models.CharField(max_length=30, default=SINGLE_OFFSET,
                                    help_text='Specific timeslot altering '
                                    'rules to use.',
                                    choices=MULTIPLICITY_CHOICES)
    single_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used to further offset '
            'the package\'s timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    single_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_hours = models.SmallIntegerField(
        default=0,
        help_text='How many hours should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_decades = models.SmallIntegerField(
        default=0,
        help_text='How many decades should the package\'s timeslot be ' 
                  'offset in order to obtain this input\'s own timeslot'
    )
    SINGLE_AGE_AGE_YOUNGEST = 'youngest'
    SINGLE_AGE_AGE_OLDEST = 'oldest'
    SINGLE_AGE_AGE_CHOICES = (
        (SINGLE_AGE_AGE_YOUNGEST, 'youngest file'),
        (SINGLE_AGE_AGE_OLDEST, 'oldest file'),
    )
    single_age_age = models.CharField(max_length=20, help_text='Relative age '
                                      'of the file',
                                      default=SINGLE_AGE_AGE_OLDEST,
                                      choices=SINGLE_AGE_AGE_CHOICES)
    SINGLE_AGE_RELATIVE_BEFORE = 'before'
    SINGLE_AGE_RELATIVE_AFTER = 'after'
    SINGLE_AGE_RELATIVE_IGNORE = 'ignore'
    SINGLE_AGE_RELATIVE_CHOICES = (
        (SINGLE_AGE_RELATIVE_BEFORE, 'before package timeslot'),
        (SINGLE_AGE_RELATIVE_AFTER, 'after package timeslot'),
        (SINGLE_AGE_RELATIVE_IGNORE, 'regardless of package timeslot'),
    )
    single_age_relative = models.CharField(max_length=20, help_text='how to '
                                           'filter the files relative to the '
                                           'package\'s timeslot?',
                                           default=SINGLE_AGE_RELATIVE_BEFORE,
                                           choices=SINGLE_AGE_RELATIVE_CHOICES)
    single_age_fix_year = models.BooleanField(default=False)
    single_age_fix_month = models.BooleanField(default=False)
    single_age_fix_day = models.BooleanField(default=False)
    single_age_fix_hour = models.BooleanField(default=False)
    single_age_fix_minute = models.BooleanField(default=False)
    multiple_arbitrary_frequency_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_hours = models.SmallIntegerField(
        default=1,
        help_text='How many hours should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_hours = models.SmallIntegerField(
        default=0,
        help_text='How many hours should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_hours = models.SmallIntegerField(
        default=0,
        help_text='How many hours should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_decadal_fix_hour = models.BooleanField(
        default=False,
        help_text='Should the hour be fixed?'
    )

    class Meta:
        verbose_name_plural = 'inputs'

    def __unicode__(self):
        return self.input.name


class OutputMembership(models.Model):
    package = models.ForeignKey(Package)
    output = models.ForeignKey(File)
    optional = models.BooleanField(default=False, help_text='Is this an '
                                   'optional output? Can the package run '
                                   'successfully without it?')
    description = models.TextField(blank=True, help_text='Description of this '
                                   'output in the context of the package.')
    except_hours = models.CommaSeparatedIntegerField(
        max_length=100,
        help_text='A list of hours, separated by commas in which this input '
            'is unavailable',
        blank=True,
    )
    offset_years = models.SmallIntegerField(default=0, help_text='How many '
                                            'years should be used to offset '
                                            'the package\'s timeslot in '
                                            'order to obtain this input\'s '
                                            'own timeslot?')
    offset_months = models.SmallIntegerField(default=0, help_text='How many '
                                             'months should be used to '
                                             'offset the package\'s '
                                             'timeslot in order to obtain '
                                             'this input\'s own timeslot?')
    offset_days = models.SmallIntegerField(default=0, help_text='How many '
                                           'days should be used to offset '
                                           'the package\'s timeslot in '
                                           'order to obtain this input\'s '
                                           'own timeslot?')
    offset_hours = models.SmallIntegerField(default=0, help_text='How many '
                                            'hours should be used to offset '
                                            'the package\'s timeslot in '
                                            'order to obtain this input\'s '
                                            'own timeslot?')
    offset_minutes = models.SmallIntegerField(default=0, help_text='How '
                                              'many minutes should be used '
                                              'to offset the package\'s '
                                              'timeslot in order to obtain '
                                              'this input\'s own timeslot?')
    offset_decades = models.SmallIntegerField(
        default=0,
        help_text='How many decades should the package\'s timeslot be ' 
                  'offset in order to obtain this input\'s own timeslot'
    )
    SINGLE_OFFSET = 'single_offset'
    SINGLE_AGE = 'single_age'
    MULTIPLE_ARBITRARY = 'multiple_arbitrary'
    MULTIPLE_DECADAL = 'multiple_decadal'
    MULTIPLICITY_CHOICES = (
        (SINGLE_OFFSET, 'single input with further timeslot offsets'),
        (SINGLE_AGE, 'single input with relative age displacement'),
        (MULTIPLE_ARBITRARY, 'multiple inputs with arbitrary interval'),
        (MULTIPLE_DECADAL, 'multiple inputs with decadal interval'),
    )
    multiplicity = models.CharField(max_length=30, default=SINGLE_OFFSET,
                                    help_text='Specific timeslot altering '
                                    'rules to use.',
                                    choices=MULTIPLICITY_CHOICES)
    single_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used to further offset '
            'the package\'s timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    single_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_hours = models.SmallIntegerField(
        default=0,
        help_text='How many hours should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used to offset the package\'s '
            'timeslot in order to obtain this input\'s own timeslot?'
    )
    single_offset_decades = models.SmallIntegerField(
        default=0,
        help_text='How many decades should the package\'s timeslot be ' 
                  'offset in order to obtain this input\'s own timeslot'
    )
    SINGLE_AGE_AGE_YOUNGEST = 'youngest'
    SINGLE_AGE_AGE_OLDEST = 'oldest'
    SINGLE_AGE_AGE_CHOICES = (
        (SINGLE_AGE_AGE_YOUNGEST, 'youngest file'),
        (SINGLE_AGE_AGE_OLDEST, 'oldest file'),
    )
    single_age_age = models.CharField(max_length=20, help_text='Relative age '
                                      'of the file',
                                      default=SINGLE_AGE_AGE_OLDEST,
                                      choices=SINGLE_AGE_AGE_CHOICES)
    SINGLE_AGE_RELATIVE_BEFORE = 'before'
    SINGLE_AGE_RELATIVE_AFTER = 'after'
    SINGLE_AGE_RELATIVE_IGNORE = 'ignore'
    SINGLE_AGE_RELATIVE_CHOICES = (
        (SINGLE_AGE_RELATIVE_BEFORE, 'before package timeslot'),
        (SINGLE_AGE_RELATIVE_AFTER, 'after package timeslot'),
        (SINGLE_AGE_RELATIVE_IGNORE, 'regardless of package timeslot'),
    )
    single_age_relative = models.CharField(max_length=20, help_text='how to '
                                           'filter the files relative to the '
                                           'package\'s timeslot?',
                                           default=SINGLE_AGE_RELATIVE_BEFORE,
                                           choices=SINGLE_AGE_RELATIVE_CHOICES)
    single_age_fix_year = models.BooleanField(default=False)
    single_age_fix_month = models.BooleanField(default=False)
    single_age_fix_day = models.BooleanField(default=False)
    single_age_fix_hour = models.BooleanField(default=False)
    single_age_fix_minute = models.BooleanField(default=False)
    multiple_arbitrary_frequency_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_hours = models.SmallIntegerField(
        default=1,
        help_text='How many hours should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_frequency_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used with the frequency offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_hours = models.SmallIntegerField(
        default=0,
        help_text='How many hours should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_start_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used with the start offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_years = models.SmallIntegerField(
        default=0,
        help_text='How many years should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_months = models.SmallIntegerField(
        default=0,
        help_text='How many months should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_days = models.SmallIntegerField(
        default=0,
        help_text='How many days should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_hours = models.SmallIntegerField(
        default=0,
        help_text='How many hours should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_arbitrary_end_offset_minutes = models.SmallIntegerField(
        default=0,
        help_text='How many minutes should be used with the end offset '
            'from the reference timeslot in order to obtain this input\'s '
            'own timeslot?'
    )
    multiple_decadal_fix_hour = models.BooleanField(
        default=False,
        help_text='Should the hour be fixed?'
    )
    should_be_archived = models.BooleanField(
        default=True,
        help_text="Should this output be archived by the autogenerated "
                  "archive task?"
    )
    should_be_cleaned = models.BooleanField(
        default=True,
        help_text="Should this output be cleaned by the autogenerated "
                  "cleanup task?"
    )
    cleaning_offset = models.IntegerField(
        default=1,
        help_text="Number of days to subtract from the input's timeslot in "
                  "order to perform a safe deletion by the autogenerated "
                  "cleanup task. This option is only used if the "
                  "'should be cleaned' option is active"
    )


    class Meta:
        verbose_name_plural = 'outputs'

    def __unicode__(self):
        return self.output.name


class MapserverPalette(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return '%s' % self.name


class PaletteRule(models.Model):
    expression = models.CharField(max_length=255, help_text='The expression '
                                  'to evaluate in mapserver style syntax')
    red = models.IntegerField(default=0, help_text='Color for the red channel')
    green = models.IntegerField(default=0,
                                help_text='Color for the green channel')
    blue = models.IntegerField(default=0,
                               help_text='Color for the blue channel')
    palette = models.ForeignKey(MapserverPalette)

    def __unicode__(self):
        return '%s - (%s, %s, %s)' % (self.expression, self.red, self.green,
                                      self.blue)


class Product(models.Model):
    name = models.CharField(max_length=100, unique=True)
    short_name = models.CharField(max_length=20, unique=True)
    version = models.CharField(max_length=20)
    description = models.TextField(blank=True, help_text='Description of the '
                                   'product. This field is used also for the '
                                   'abstract element of the metadata records.')
    palette = models.ForeignKey('MapserverPalette', help_text='The pallete '
                                'that will be used when creatings quicklooks '
                                'and WMS layers for this product')
    point_of_contact = models.ForeignKey('Collaborator',
                                         related_name='point_of_contact+',
                                         help_text='Metadata point of contact')
    principal_investigator = models.ForeignKey(
        'Collaborator',
        related_name='principal_investigator+'
    )
    originator = models.ForeignKey('Collaborator',
                                    related_name='originator+')
    distributor = models.ForeignKey('Collaborator',
                                    related_name='distributor+')
    owner = models.ForeignKey('Collaborator', related_name='owner+')
    responsible = models.ForeignKey('Collaborator',
                                    related_name='responsible+')
    # new fields with metadata information
    parent_identifier = models.CharField(max_length=255, help_text='UUID of '
                                         'the metadata series where this '
                                         'product belongs.')
    sdi_service_id = models.CharField(max_length=255, help_text='Service id '
                                      'used by VITO\'s SDI to refer to this '
                                      'product\'s service.')
    user_manual_url = models.CharField(max_length=255, help_text='URL for the '
                                       'Product User Manual')
    INT_TYPE = 'int'
    FLOAT_TYPE = 'float'
    CONVERSION_DTYPE_CHOICES = (
        (INT_TYPE, INT_TYPE),
        (FLOAT_TYPE, FLOAT_TYPE),
    )
    geotiff_dtype = models.CharField(max_length=15, help_text='Data type to '
                                     'use when converting the HDF5 product to '
                                     'other file formats',
                                     choices=CONVERSION_DTYPE_CHOICES)
    inspire_data_theme = models.CharField(max_length=100, help_text='INSPIRE '
                                          'data theme, as described in the '
                                          'GEMET Thesaurus available online '
                                          'at http://www.eionet.europa.eu/'
                                          'gemet/inspire_themes')
    gemet_keywords = models.CharField(max_length=255, help_text='Other '
                                      'descriptive keywords from the '
                                      'GEMET thesaurus. Multiple keywords must'
                                      ' be separated with commas.',null=True,
                                      blank=True)
    other_keywords = models.CharField(max_length=255, help_text='Other '
                                      'descriptive keywords not found on the '
                                      'GEMET thesaurus. Multiple keywords must'
                                      ' be separated with commas.', null=True,
                                      blank=True)
    iso_19115_topic_categories = models.CharField(
        max_length=255,
        help_text='ISO 19115 topic categories. Multiple categories should be '
                  'separated with commas.'
    )
    pixel_size = models.FloatField(help_text='Ground resolution of product '
                                   'pixels expressed in decimal degree units.')
    coordinate_reference_system_epsg_code = models.SmallIntegerField(
        help_text='EPSG code for the coordinate reference system of this '
                  'product. Codes can be consulted online at: http://'
                  'spatialreference.org/'
    )
    credit = models.TextField(help_text='Product credits')
    purpose = models.TextField(help_text='Product purpose')
    graphic_overview_description = models.CharField(max_length=100,
                                                    help_text='Description '
                                                    'of the graphical '
                                                    'overview')
    PNG_TYPE = 'png'
    WMS_TYPE = 'wms'
    GRAPHIC_OVERVIEW_TYPE_CHOICES = (
        (PNG_TYPE, PNG_TYPE),
        (WMS_TYPE, WMS_TYPE),
    )
    graphic_overview_type = models.CharField(
        max_length=10, help_text='Format of the quicklooks',
        default=PNG_TYPE, choices=GRAPHIC_OVERVIEW_TYPE_CHOICES
    )
    supplemental_info = models.CharField(max_length=255, help_text='URL for '
                                         'the product page', blank=True,
                                         null=True)
    validation_report_url = models.CharField(max_length=255, help_text='URL '
                                             'for the validation report')
    lineage = models.TextField(help_text='General explanation of the data '
                               'producer\'s knowledge about the lineage of a '
                               'dataset. Apart from describing the process '
                               'history, the overall quality of the dataset '
                               '(series) should be included in the Lineage '
                               'metadata element. This statement should '
                               'contain any quality information required for '
                               'interoperability and/or valuable for use and '
                               'evaluation of the data set (series)')
    sources = models.ManyToManyField('Source', help_text='Sources used in '
                                     'data fusion process for generating this '
                                     'product')
    temporal_extent = models.CharField(max_length=100, help_text='Description '
                                       'of the temporal extent')

    def __unicode__(self):
        return '%s' % self.name


class Dataset(models.Model):
    COVERAGE_CONTENT_TYPE_CHOICES = (
        ('image', 'image'),
        ('thematicClassification', 'thematicClassification'),
        ('physicalMeasurement', 'physicalMeasurement')
    )
    name = models.CharField(max_length=100)
    hdf5_path = models.CharField(max_length=255, help_text='Path to '
                                 'the dataset inside the HDF5 file.')
    product = models.ForeignKey(Product)
    description = models.TextField(null=True, blank=True)
    unit = models.CharField(max_length=20, blank=True, null=True)
    main_dataset = models.BooleanField(default=False, help_text='Is this the '
                                       'product\'s main dataset?')
    scaling_factor = models.IntegerField()
    missing_value = models.IntegerField()
    coverage_content_type = models.CharField(
        max_length=30,
        help_text='INSPIRE metadata element: choices:<br />Image - '
                  'Meaningful numerical representation of a physical '
                  'parameter that is not the actual value of the '
                  'physical parameter.<br />thematicClassification - '
                  'code value with no quantitative meaning, used to '
                  'represent a physical quantity.<br />'
                  'physicalMeasurement - Value in physical units of the '
                  'quantity being measured.',
        choices=COVERAGE_CONTENT_TYPE_CHOICES
    )
    max_value = models.IntegerField(default=0)
    min_value = models.IntegerField(default=0)
    bit_depth= models.IntegerField(default=16)

    def __unicode__(self):
        return self.name


class Host(models.Model):
    name = models.CharField(max_length=100, unique=True)
    domain = models.CharField(max_length=100, blank=True, default='meteo.pt',
                              help_text='Network domain for this host')
    description = models.TextField(blank=True)
    active = models.BooleanField(default=True, help_text='Is this host '
                                 'currently active?')
    ip = models.GenericIPAddressField()
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=20)
    temp_dir = models.CharField(max_length=200, help_text='The base directory '
                                'for storing temporary files on this host.',
                                blank=True)
    data_dir = models.CharField(max_length=200, help_text='The base directory '
                                'where data is generally stored on this host.',
                                blank=True)
    code_dir = models.CharField(max_length=200, help_text='The base directory '
                                'where code is generally stored on this host.',
                                blank=True)

    def role_names(self):
        role_managers = [
            self.iobufferhost_set,
            self.archivehost_set,
            self.webserverhost_set,
            self.externalftphost_set,
            self.orderinghost_set,
        ]
        roles = []
        for manager in role_managers:
            try:
                roles.append(manager.get().__class__.__name__)
            except ObjectDoesNotExist:
                pass
        return ', '.join(roles)
    role_names.short_description = 'Roles'

    def __unicode__(self):
        return '%s' % self.name


class Source(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return '%s' % self.name


class SourceSearchPattern(models.Model):
    pattern = models.CharField(max_length=200, help_text='A regular '
                               'expression string with added special '
                               'marks to use when parsing', unique=True)
    source = models.ForeignKey(Source)

    def __unicode__(self):
        return '%s' % self.pattern


class SourceParameter(models.Model):
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100)
    source = models.ForeignKey(Source)

    class Meta:
        verbose_name_plural = 'parameters'

    def __unicode__(self):
        return '%s: %s' % (self.name, self.value)


class Suite(models.Model):
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    default_host = models.ForeignKey(Host, help_text='Host where the pacakges '
                                     'will be executed',
                                     related_name='default_host')
    packages = models.ManyToManyField(Package, help_text='choose the packages '
                                      'that are part of this suite',
                                      blank=True)
    use_io_buffer_for_searching = models.BooleanField(
        default=True,
        help_text='Should the io_buffer_host be used when searching for '
                  'package input files?'
    )
    copy_outputs_to_io_buffer = models.BooleanField(
        default=True,
        help_text='Should the outputs of each package be copied to the '
                  'io_buffer host? This option, used in conjunction with the '
                  '"use_io_buffer_for_searching" option is a good way to '
                  'ensure that the files are accessible to multiple hosts '
                  'without constantly searching the archive'
    )
    use_archive_for_searching = models.BooleanField(
        default=False,
        help_text='Should the archive_host be used when searching for '
                  'package input files?'
    )
    copy_outputs_to_archive = models.BooleanField(
        default=False,
        help_text='Should the package\'s outputs be copied to the archive '
                  'host after execution? This option should not be used '
                  'together with the "create_archive_family" because it is'
                  'redundant.'
    )
    extra_hosts_to_copy_outputs = models.ManyToManyField(
        Host,
        blank=True,
        help_text='Send the package\'s outputs to extra hosts'
    )
    remove_package_working_dir = models.BooleanField(
        default=True,
        help_text='Delete each package\'s working directory after ' \
                  'execution? This option is useful mainly for testing ' \
                  'new packages and should usually be kept on to prevent ' \
                  'the file systems from filling up.'
    )
    INTERVAL = 'date interval'
    LIST = 'list of individual dates'
    DATE_CHOICES = (
        (INTERVAL, INTERVAL),
        (LIST, LIST),
    )
    date_mode = models.CharField(max_length=100, choices=DATE_CHOICES,
                                 default=INTERVAL, help_text='Suite date '
                                 'modes: mode "%s" means that the suite will '
                                 'use an ecFlow repeat date with the chosen '
                                 'start_date and end_date as interval bounds. '
                                 'Mode %s means that the suite will use an '
                                 'ecFlow repeat string with the values '
                                 'supplied by the date_list field.' 
                                 % (INTERVAL, LIST))
    start_date = models.DateField(null=True, blank=True, help_text='If this '
                                 'suite should have a start date, what date '
                                 'should it be? This field is only used if '
                                 'the date_mode field\'s value is %s' 
                                  % INTERVAL)
    end_date = models.DateField(null=True, blank=True, help_text='If this '
                                'suite should have an end date, what date '
                                'should it be? This field is only used if '
                                'the date_mode field\'s value is %s' 
                                % INTERVAL)
    date_list = models.CharField(max_length=255, blank=True, help_text='List '
                                 'of dates to use in the suite. This should '
                                 'be a comma-separated list of YYYYMMDD '
                                 'strings. This field is only used if the '
                                 'date_mode field\'s value is %s' % LIST)
    NRT = "NRT"
    REPROCESSING = "Reprocessing"
    OP_MODE_CHOICES = (
        (NRT, NRT),
        (REPROCESSING, REPROCESSING)
    )
    operation_mode = models.CharField(max_length=20, choices=OP_MODE_CHOICES,
                                      default=NRT, help_text="NRT mode means "
                                      "that there are time triggers in place "
                                      "that control the start of each hour. "
                                      "Reprocessing means that there are no "
                                      "temporal triggers in the suite, "
                                      "every triggers is based on "
                                      "dependencies. Also, in NRT mode the "
                                      "archive and clean families (if "
                                      "present) are created outside of the "
                                      "main family, with their own repeat.")
    operations_database_url = models.CharField(max_length=255, help_text='URL '
                                               'for the application that stores'
                                               ' operations statuses and '
                                               'information', blank=True)
    operations_database_user = models.CharField(max_length=255,
                                                help_text='name of the user'
                                                'for the application that '
                                                'stores operations statuses '
                                                'and information', blank=True)
    operations_database_api_key = models.CharField(max_length=255,
                                                   help_text='API key of the '
                                                   'user for the application '
                                                   'that stores operations '
                                                   'statuses and information',
                                                   blank=True)
    collaborators_to_email = models.ManyToManyField(
        'Collaborator',
        blank=True,
        limit_choices_to={'organization__short_name':'IPMA'},
        help_text='List of people that will be notified via e-mail when a '
                  'task fails or returns a false result'
    )
    create_cleaning_family = models.BooleanField(
        default=True, 
        help_text='Should there be a dedicated family for performing cleaning '
        'of the execution hosts?'
    )
    create_archive_family = models.BooleanField(
        default=True,
        help_text='Should there be a dedicated family for archiving the '
        'outputs?'
    )
    archive_from_io_buffer = models.BooleanField(
        default=True,
        help_text='Should the archive family use the io_buffer as its '
	    'execution host? This option should be used in operational '
        'processing lines that use the io_buffer host role.'
    )
    archive_time = models.TimeField(
        default = dt.time(4, 30),
        null=True,
        blank=True,
        help_text='At what time of the day should the archive family run?'
    )

    def __unicode__(self):
        return self.name


# There is no need to specify settings such as archive periodicity
# or archive time because in the future there shall be packages 
# that perform maintenance on the io buffer host. 
# This maintenance will consist of:
# - send data to the archive
# - delete old data after some space constraint is reached
class IOBufferHost(models.Model):
    host = models.ForeignKey(Host)
    clean_up_threshold = models.SmallIntegerField(default=90,
                                                  help_text='Disk usage '
                                                  'threshold. When the '
                                                  'percentage of occupied '
                                                  'space is bigger than this '
                                                  'the host will start '
                                                  'issuing alert emails '
                                                  'and warning messages.')

    class Meta:
        verbose_name_plural = 'IO Buffer Host'

    def __unicode__(self):
        return self.host.name


class ArchiveHost(models.Model):
    host = models.ForeignKey(Host)
    inputs_base_dir = models.CharField(max_length=255, help_text='Path to '
                                       'the root directory where input data '
                                       'is stored.')
    outputs_base_dir = models.CharField(max_length=255, help_text='Path to '
                                        'the root directory where output data '
                                        'is stored.')
    backups_base_dir = models.CharField(max_length=255, help_text='Path to '
                                        'the root directory where backup data '
                                        'is stored.')

    class Meta:
        verbose_name_plural = 'Archive Host'

    def __unicode__(self):
        return self.host.name


# This host must hold settings for all the services accessible via Web,
# such as the CSW catalogue, WMS service, OSEO service, quickloooks, etc
class WebserverHost(models.Model):
    host = models.ForeignKey(Host)
    base_url = models.CharField(max_length=255, help_text='base URL for '
                                'accessing giosystem web data. It is appended '
                                'to the host\'s name')
    catalogue_username = models.CharField(max_length=255, help_text='name of '
                                          'the user with permission to edit '
                                          'the catalogue')
    catalogue_password = models.CharField(max_length=255, help_text='password '
                                          'of the user with permission to edit'
                                          ' the catalogue')
    catalogue_base_uri = models.CharField(max_length=255, help_text='CSW '
                                          'catalogue base URI. It is '
                                          'appended to the base_url field')
    catalogue_csw_search_uri = models.CharField(max_length=255,
                                                help_text='URI for CSW '
                                                'discovery operations. It '
                                                'is appended to '
                                                'catalogue_base_uri.')
    catalogue_csw_transaction_uri = models.CharField(max_length=255, 
                                                     help_text='URI for CSW '
                                                     'insert operations. It '
                                                     'is appended to '
                                                     'catalogue_base_uri.')
    static_quicklook_base_uri = models.CharField(max_length=255,
                                                 help_text='URI for accessing '
                                                 'static quicklooks. It is '
                                                 'appended to base_url.')
    ows_base_uri = models.CharField(max_length=255, help_text='URI for '
                                    'accessing OGS OWS services, like WMS. '
                                    'It is appended to base_url')

    class Meta:
        verbose_name_plural = 'Web Server Host'

    def __unicode__(self):
        return self.host.name


class EcflowServer(models.Model):
    name = models.CharField(max_length=100, unique=True, help_text='Name for '
                            'the ecflow server. This name is used for '
                            'creating directories on the host containing the '
                            'ecflow files. For each ecflow server, the '
                            'following directory structures will be created '
                            'in the host: '
                            '{code_dir}/ecflow/{name}/server_files and '
                            '{code_dir}/ecflow/{name}/suites')
    host = models.ForeignKey(Host)
    port_number = models.IntegerField(default=3141, help_text='Port number '
                                      'for the ecFlow server.')
    suites = models.ManyToManyField('Suite', help_text='What suites should '
                                    'run on this ecFlow server instance?')

    class Meta:
        verbose_name = 'ecFlow server'
        verbose_name_plural = 'ecFlow Servers'

    def __unicode__(self):
        return self.name


class ExternalFTPHost(models.Model):
    host = models.ForeignKey(Host)

    class Meta:
        verbose_name_plural = 'External FTP Host'

    def __unicode__(self):
        return self.host.name


class OrderingHost(models.Model):
    host = models.ForeignKey(Host)

    class Meta:
        verbose_name_plural = 'Ordering Host'

    def __unicode__(self):
        return self.host.name


class DataReceiverHost(models.Model):
    host = models.ForeignKey(Host)
    inputs_base_dir = models.CharField(max_length=255, help_text='Path to '
                                       'the root directory where input data '
                                       'arrives.')

    class Meta:
        verbose_name_plural = 'Data Receiver Host'

    def __unicode__(self):
        return self.host.name


class Logging(models.Model):
    DEBUG = 'DEBUG'
    INFO = 'INFO'
    WARNING = 'WARNING'
    ERROR = 'ERROR'
    LOG_LEVEL_CHOICES = (
        (DEBUG, DEBUG),
        (INFO, INFO),
        (WARNING, WARNING),
        (ERROR, ERROR),
    )
    log_level = models.CharField(max_length=10, choices=LOG_LEVEL_CHOICES,
                                 default=INFO, help_text='Level of the log')
    log_format = models.CharField(max_length=200, default='%(name)s '
                                  '%(levelname)s %(message)s',
                                  help_text='format string for the log '
                                  'messages')

    class Meta:
        verbose_name_plural = 'Logging'

    def __unicode__(self):
        return self.log_level


class Organization(models.Model):
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=50)
    url = models.CharField(max_length=255)
    street_address = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    e_mail = models.EmailField(max_length=255)

    def __unicode__(self):
        return self.name


class HomeOrganization(models.Model):
    organization = models.ForeignKey(Organization)

    class Meta:
        verbose_name_plural = 'Home Organization'

    def __unicode__(self):
        return self.organization.name


class Collaborator(models.Model):
    name = models.CharField(max_length=255)
    e_mail = models.EmailField(max_length=255)
    organization = models.ForeignKey(Organization)

    def __unicode__(self):
        return '{} - {}'.format(self.organization.short_name, self.name)


class GeographicArea(models.Model):
    name = models.CharField(max_length=50)
    upper_latitude = models.FloatField()
    lower_latitude = models.FloatField()
    leftmost_longitude = models.FloatField()
    rightmost_longitude = models.FloatField()

    def __unicode__(self):
        return self.name
