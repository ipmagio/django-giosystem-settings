from django.conf.urls import patterns, include, url
import tastypie.api

from coresettings import views
from coresettings.api import resources

v1_api = tastypie.api.Api(api_name='v1')
v1_api.register(resources.HostResource())
v1_api.register(resources.SourceResource())
v1_api.register(resources.FileResource())
v1_api.register(resources.MapserverPaletteResource())
v1_api.register(resources.ProductResource())
v1_api.register(resources.PackageResource())
v1_api.register(resources.SuiteResource())
v1_api.register(resources.EcflowServerResource())
v1_api.register(resources.IOBufferHostResource())
v1_api.register(resources.ArchiveHostResource())
v1_api.register(resources.WebserverHostResource())
v1_api.register(resources.ExternalFTPHostResource())
v1_api.register(resources.OrderingHostResource())
v1_api.register(resources.DataReceiverHostResource())
v1_api.register(resources.LoggingResource())
v1_api.register(resources.OrganizationResource())
v1_api.register(resources.HomeOrganizationResource())
v1_api.register(resources.GeographicAreaResource())

urlpatterns = patterns(
    '',
    url(r'^graph/packages/$', views.create_packages_graph,
        {'include_files': False}, name='coresettings-packages_graph'),
    url(r'^graph/packages_files/$', views.create_packages_graph,
        {'include_files': True}, name='coresettings-packages_files_graph'),
    url(r'^api/', include(v1_api.urls)),
)
