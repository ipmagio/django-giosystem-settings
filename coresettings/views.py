from django.http import HttpResponse, Http404

import networkx as nx
import pygraphviz as pgv

import coresettings.models as cm

def create_packages_graph(request, include_files=True, include_categories=True,
                          package_color='dodgerblue', file_color='gray'):
    graph = cm.Package.create_graph()
    if include_files:
        the_graph = graph
    else:
        packages = [n[0] for n in graph.nodes(data=True) if \
                n[1]['type_'] == 'package']
        the_graph = graph.subgraph(packages)
    a_graph = nx.to_agraph(the_graph)
    for n in the_graph.nodes_iter():
        a_node = a_graph.get_node(n)
        if the_graph.node[n].get('type_') == 'file':
            a_node.attr['color'] = file_color
            a_node.attr['fontsize'] = 8
        elif the_graph.node[n].get('type_') == 'package':
            a_node.attr['color'] = 'black'
            a_node.attr['fillcolor'] = package_color
            a_node.attr['shape'] = 'box'
            a_node.attr['style'] = 'filled, rounded'
            a_node.attr['fontcolor'] = 'white'
    for e in the_graph.edges_iter():
        n1, n2 = e
        a_edge = a_graph.get_edge(n1, n2)
        n1_type = the_graph.node[n1].get('type_')
        n2_type = the_graph.node[n2].get('type_')
        if n1_type == 'file' or n2_type == 'file':
            a_edge.attr['style'] = 'dotted'
            a_edge.attr['color'] = file_color
            a_edge.attr['arrowhead'] = 'dot'
            a_edge.attr['arrowsize'] = 0.5
        elif n1_type == 'package' and n1_type == 'package':
            a_edge.attr['color'] = package_color
    if include_categories:
        for cat in cm.PackageCategory.objects.all():
            cat_packages = [p.name for p in cat.package_set.all()]
            a_graph.add_subgraph(cat_packages, 'cluster_%s' % cat, label=cat,
                                 color='gray90', style='filled, rounded')
    response = HttpResponse(content_type='image/png')
    a_graph.draw(response, format='png', prog='dot')
    return response

def create_suite_graph(suite):
    raise NotImplementedError
